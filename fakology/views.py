from django.shortcuts import render
from django.http import HttpResponse
from joblib import dump, load

profileURL = ''

def index(request):
   return render(request, 'fakology/index.html')

def result(request):
   try:
      user = getinfo('http://instagram.com/{}'.format(request.POST['username']))
      svm_classifier = load('SVM_model.joblib')
      tree_classifier = load('tree_model.joblib')
      MLP_classifier = load('MLP_model.joblib')
      standard_scaler = load('scaler.joblib')

      user_std = standard_scaler.transform([user])
      svm_verdict = svm_classifier.predict(user_std)
      tree_verdict = tree_classifier.predict([user])
      MLP_verdict = MLP_classifier.predict(user_std)

      context = {
         'tree': tree_verdict,
         'svm': svm_verdict,
         'nn': MLP_verdict,
         'username': request.POST['username'],
         'picURL': profileURL,
      }

      return render(request, 'fakology/result.html', context)

   except:
      return HttpResponse('Username does not exist!')

import requests, string, csv, ssl, json, time
from random import randint
import urllib.request
import urllib.parse
import urllib.error
from bs4 import BeautifulSoup
import random

def getinfo(url):
    global profileURL
    html = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(html, 'html.parser')

    body = soup.find('body')
    script_tag = body.find('script')
    raw_string = script_tag.text.strip().replace('window._sharedData =', '').replace(';', '')
    js = json.loads(raw_string)

    username = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["username"]
    fullname = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["full_name"]
    description = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["biography"]
    exURL = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["external_url"]
    private = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["is_private"]
    noPosts = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["count"]
    noFollowers = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_followed_by"]["count"]
    noFollows = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_follow"]["count"]
    profileURL = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["profile_pic_url"]

    length_username = len(username)
    nums_username = sum(c.isdigit() for c in username)
    nums_fullname = sum(c.isdigit() for c in fullname)
    if len(fullname):
        ratio_nums_fullname = nums_fullname/len(fullname)
    else:
        ratio_nums_fullname = 0
    if exURL:
        exURL = 1
    else:
        exURL = 0
    if private:
        private = 1
    else:
        private = 0

    if len(fullname):
        nameWords = sum(c.isspace() for c in fullname) + 1
    else:
        nameWords = 0

    if fullname==username:
        fullnameEqualsUsername = 1
    else:
        fullnameEqualsUsername = 0

    if "/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht" in profileURL:
        profile_pic = 0
    else:
        profile_pic = 1

    return profile_pic, round(nums_username/length_username,2), nameWords, round(ratio_nums_fullname,2), fullnameEqualsUsername, len(description), exURL, private, noPosts, noFollowers, noFollows


# print(1 == clf.predict([[pp, rnu, nw, rnf, neu, dl, eurl, pr, p, frs, fls]]))
# print(clf.predict([[pp, rnu, nw, rnf, neu, dl, eurl, pr, p, frs, fls]])):
#     print('fake')

