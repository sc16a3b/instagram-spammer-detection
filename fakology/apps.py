from django.apps import AppConfig


class FakologyConfig(AppConfig):
    name = 'fakology'
