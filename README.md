# Fakology

Fakology is a Django web app to demo machine learning models capable of detecting Instagram spammers/fakes.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install Fakology.

on a DEC10 machine install Python 3.
```bash
$ module add anaconda3
```

Create a virtual environment. *vEnv* is the name of the virtual environment, it can be anything.
```bash
$ python -m venv vEnv
```
cd into the virtual environment directory and activate it.
```bash
$ cd vEnv
$ source bin/activate
```
Install Django and joblib.
```bash
$ pip install django
$ pip install joblib
```
Next clone the project into the virtual environment.
```bash
$ git clone git@gitlab.com:sc16a3b/instagram-spammer-detection.git
```

## Usage

cd into the cloned directory and run the app.
```bash
$ cd instagram-spammer-detection
$ python manage.py runserver
```
You can now visit the app's homepage at:

http://127.0.0.1:8000/fakology






