import requests, string, csv, ssl, json, time
from random import randint
import urllib.request
import urllib.parse
import urllib.error
from bs4 import BeautifulSoup


class Insta_Info_Scraper:
	
	def getinfo(self, url):
		html = urllib.request.urlopen(url, context=self.ctx).read()
		soup = BeautifulSoup(html, 'html.parser')

		body = soup.find('body')
		script_tag = body.find('script')
		raw_string = script_tag.text.strip().replace('window._sharedData =', '').replace(';', '')
		js = json.loads(raw_string)

		username = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["username"]
		#   print("Username: {}".format(username))
		fullname = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["full_name"]
		#   print("Fullname: {}".format(fullname))
		description = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["biography"]
		#   print("Description: {}".format(description))
		exURL = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["external_url"]
		#   print("External URL: {}".format(url))
		private = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["is_private"]
		#   print("Private: {}".format(private))
		noPosts = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["count"]
		#   print("# Posts: {}".format(noPosts))
		noFollowers = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_followed_by"]["count"]
		#   print("# Followers: {}".format(noFollowers))
		noFollows = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_follow"]["count"]
		#   print("# Follows: {}".format(noFollows))
		profileURL = js["entry_data"]["ProfilePage"][0]["graphql"]["user"]["profile_pic_url"]
		#   print("profile url: {}".format(profileURL))
		length_username = len(username)
		nums_username = sum(c.isdigit() for c in username)
		nums_fullname = sum(c.isdigit() for c in fullname)
		if len(fullname):
			ratio_nums_fullname = nums_fullname/len(fullname)
		else:
			ratio_nums_fullname = 0
		if exURL:
			exURL = 1
		else:
			exURL = 0
		if private:
			private = 1
		else:
			private = 0
		# not informative enough and commented out
		# unique = []
		# for char in username[::]:
		# 	if char not in unique:
		# 		unique.append(char)
		# ratio_unique_letter_username = len(unique)/length_username
		#   print(f"Ratio_unique_letter_username of unique letters in username to length of it: {ratio_unique_letter_username:.2f}")
		if len(fullname):
			nameWords = sum(c.isspace() for c in fullname) + 1
		else:
			nameWords = 0

		if fullname==username:
			fullnameEqualsUsername = 1
		else:
			fullnameEqualsUsername = 0
		#   print("Does name equal username? {}".format(fullname==username))
		#   print("Description length: {}".format(len(description)))

		if "/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht" in profileURL:
			profile_pic = 0
		else:
			profile_pic = 1
		#   print("Has prfoile pic? {}".format(profile_pic))
		return profile_pic, round(nums_username/length_username,2), nameWords, round(ratio_nums_fullname,2), fullnameEqualsUsername, len(description), exURL, private, noPosts, noFollowers, noFollows

	def main(self):
		self.ctx = ssl.create_default_context()
		self.ctx.check_hostname = False
		self.ctx.verify_mode = ssl.CERT_NONE

		with open('header.csv', mode='w') as td:
			training_writer = csv.writer(td, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			training_writer.writerow(['profile pic', 'nums/length username', 'fullname words',
											'nums/length fullname' ,'name==username', 'description length', 'external URL', 'private', '#posts', '#followers', '#follows', 'fake'])
			with open('gen_rest.txt') as f:
				self.content = f.readlines()
				self.content = [x.strip() for x in self.content]
				for idx, url in enumerate(self.content):
					rnd = randint(5, 8)
					try:
						pp, rnu, nw, rnf, neu, dl, eurl, pr, p, frs, fls = self.getinfo('http://instagram.com/{}'.format(url))
						training_writer.writerow([pp, rnu, nw, rnf, neu, dl, eurl, pr, p, frs, fls, '0'])
						print("{} done, sleeping for: {}".format(idx,rnd))
					except:
						print("An exception occurred")
					time.sleep(rnd)
				
			print("\n\nfinished genuines!\n\n")
			with open('fak_rest.txt') as f:
				self.content = f.readlines()
				self.content = [x.strip() for x in self.content]
				for idx, url in enumerate(self.content):
					rnd = randint(5, 8)
					try:
						pp, rnu, nw, rnf, neu, dl, eurl, pr, p, frs, fls = self.getinfo('http://instagram.com/{}'.format(url))
						training_writer.writerow([pp, rnu, nw, rnf, neu, dl, eurl, pr, p, frs, fls, '1'])
						print("{} done, sleeping for: {}".format(idx,rnd))
					except:
						print("An exception occurred")
					time.sleep(rnd)


if __name__ == '__main__':
	obj = Insta_Info_Scraper()
	obj.main()
